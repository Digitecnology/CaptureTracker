#include "kinectframelistener.h"

KinectFrameListener::KinectFrameListener(QObject *parent) : QObject(parent), m_image(0)
{

}

void KinectFrameListener::onNewFrame(openni::VideoStream& stream)
{
	stream.readFrame(&this->m_frame);

	//Write image
	switch(stream.getVideoMode().getPixelFormat())
	{
		case openni::PIXEL_FORMAT_DEPTH_100_UM:
		case openni::PIXEL_FORMAT_DEPTH_1_MM:
			for(int i = 0; i < this->m_image->width() * this->m_image->height(); i++)
				this->m_image->bits()[i] = ((((quint16 *)this->m_frame.getData())[i]) * 255) / 10000;
			break;
		case openni::PIXEL_FORMAT_GRAY8:
		case openni::PIXEL_FORMAT_GRAY16:
			for(int i = 0; i < this->m_image->width() * this->m_image->height(); i++)
				this->m_image->bits()[i] = ((((quint16 *)this->m_frame.getData())[i]) * 255) / 65535;
			break;
		case openni::PIXEL_FORMAT_RGB888:
			//for(int i = 0; i < this->m_image->width() * this->m_image->height() * 3; i++)
			//	this->m_image->bits()[i] = ((quint8 *)this->m_frame.getData())[i];
			memcpy(this->m_image->bits(), this->m_frame.getData(), this->m_image->width() * this->m_image->height() * 3);
			break;
	}

	emit onNewFrame();
}

QImage *KinectFrameListener::getImage()
{
	return this->m_image;
}

openni::VideoFrameRef *KinectFrameListener::getVideoFrame()
{
	return &this->m_frame;
}

void KinectFrameListener::init(openni::VideoMode mode)
{
	QImage::Format format;

	switch(mode.getPixelFormat())
	{
		case openni::PIXEL_FORMAT_DEPTH_100_UM:
		case openni::PIXEL_FORMAT_DEPTH_1_MM:
		case openni::PIXEL_FORMAT_GRAY8:
		case openni::PIXEL_FORMAT_GRAY16:
			format = QImage::Format_Grayscale8;
			break;
		case openni::PIXEL_FORMAT_RGB888:
			format = QImage::Format_RGB888;
			break;
		deafult: // TODO: Implement other pixel formats
			format = QImage::Format_RGB888;
			break;
	}

	this->m_image = new QImage(mode.getResolutionX(), mode.getResolutionY(), format);
}

void KinectFrameListener::destroy()
{
	this->m_image = 0;
}
