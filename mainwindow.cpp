#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	QList<QString> devices = this->ui->widget_camera->getAvailableDevices();

	for(QString device : devices)
		this->ui->comboBox_devices->addItem(device);
}

MainWindow::~MainWindow()
{
	delete ui;
}


void MainWindow::quit()
{
	if(this->ui->widget_camera->isStarted())
		this->ui->widget_camera->stop();

	QApplication::quit();
}

void MainWindow::startAndStop()
{
	if(!this->ui->widget_camera->isOpened())
		return;

	if(this->ui->widget_camera->isStarted())
	{
		// Stop
		this->ui->widget_camera->stop();
		this->ui->pushButton_startstop->setText(QString("Start"));
		this->ui->comboBox_ir->setEnabled(true);
		this->ui->comboBox_color->setEnabled(true);
		this->ui->comboBox_depth->setEnabled(true);
	}
	else
	{
		// Set IR and color modes
		this->ui->widget_camera->setIRMode(this->ui->comboBox_ir->currentIndex());
		this->ui->widget_camera->setColorMode(this->ui->comboBox_color->currentIndex());
		this->ui->widget_camera->setDepthMode(this->ui->comboBox_depth->currentIndex());

		// Start
		this->ui->widget_camera->start();

		if(this->ui->widget_camera->isStarted())
		{
			this->ui->pushButton_startstop->setText(QString("Stop"));
			this->ui->comboBox_ir->setEnabled(false);
			this->ui->comboBox_color->setEnabled(false);
			this->ui->comboBox_depth->setEnabled(false);
		}
		else
			this->ui->pushButton_startstop->setText(QString("Start"));
	}
}


void MainWindow::connectAndDisconnect()
{
	if(this->ui->widget_camera->isStarted())
		this->startAndStop();

	if(this->ui->widget_camera->isOpened())
	{
		// Close
		this->ui->widget_camera->close();
		this->ui->comboBox_ir->clear();
		this->ui->comboBox_color->clear();
		this->ui->comboBox_depth->clear();
		this->ui->pushButton_startstop->setEnabled(false);
		this->ui->pushButton_openclose->setText(QString("Connect"));
		this->ui->comboBox_devices->setEnabled(true);
	}
	else
	{
		// Open
		this->ui->widget_camera->open(this->ui->comboBox_devices->currentText());

		if(this->ui->widget_camera->isOpened())
		{
			this->ui->pushButton_openclose->setText(QString("Disconnect"));

			// Fill IR and Color modes
			this->ui->comboBox_ir->clear();

			for(QString mode : this->ui->widget_camera->getAvailableIRModes())
				this->ui->comboBox_ir->addItem(mode);

			this->ui->comboBox_color->clear();

			for(QString mode : this->ui->widget_camera->getAvailableColorModes())
				this->ui->comboBox_color->addItem(mode);

			this->ui->comboBox_depth->clear();

			for(QString mode : this->ui->widget_camera->getAvailableDepthModes())
				this->ui->comboBox_depth->addItem(mode);

			this->ui->pushButton_startstop->setEnabled(true);
			this->ui->comboBox_devices->setEnabled(false);
		}
		else
			this->ui->pushButton_openclose->setText(QString("Connect"));
	}
}

void MainWindow::showIRToggled(bool value)
{
	if(value)
		this->ui->widget_camera->setCurrentSensor(KinectSensorType::KINECT_SENSOR_IR);
}

void MainWindow::showColorToggled(bool value)
{
	if(value)
		this->ui->widget_camera->setCurrentSensor(KinectSensorType::KINECT_SENSOR_COLOR);
}

void MainWindow::showDepthToggled(bool value)
{
	if(value)
		this->ui->widget_camera->setCurrentSensor(KinectSensorType::KINECT_SENSOR_DEPTH);
}

void MainWindow::showSkeletonBonesChanged(bool value)
{
	this->ui->widget_camera->setShowSkeletonBones(value);
}

void MainWindow::showSkeletonMapChanged(bool value)
{
	this->ui->widget_camera->setShowSkeletonMap(value);
}
