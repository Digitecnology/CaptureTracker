#include "kinectbodyframelistener.h"

KinectBodyFrameListener::KinectBodyFrameListener(QObject *parent) : QObject(parent), m_usermap(0), m_bones(0)
{

}

void KinectBodyFrameListener::onNewFrame(nite::UserTracker &tracker)
{
	tracker.readFrame(&this->m_trackframe);

	if(!m_bones || !m_usermap)
		return;

	//Draw UserMap image
	for(int i = 0; i < this->m_usermap->width() * this->m_usermap->height(); i++)
	{
		switch(this->m_trackframe.getUserMap().getPixels()[i])
		{
			case 0:
				((quint32 *)this->m_usermap->bits())[i] = 0x00000000;
				break;
			case 1:
				((quint32 *)this->m_usermap->bits())[i] = 0xCCFF0000;
				break;
			case 2:
				((quint32 *)this->m_usermap->bits())[i] = 0xCCFFFFCC;
				break;
			case 3:
				((quint32 *)this->m_usermap->bits())[i] = 0xCC00FF00;
				break;
			case 4:
				((quint32 *)this->m_usermap->bits())[i] = 0xCC0000FF;
				break;
			case 5:
				((quint32 *)this->m_usermap->bits())[i] = 0xCC00FFFF;
				break;
			case 6:
				((quint32 *)this->m_usermap->bits())[i] = 0xCCFFFF00;
				break;
			default:
				break;
		}
	}

	const nite::Array<nite::UserData> *users = &this->m_trackframe.getUsers();

	// Draw skeletons
	nite::Point3f joint_pos;
	float joint_pos_depth_x;
	float joint_pos_depth_y;
	float temp1_x;
	float temp1_y;
	float temp2_x;
	float temp2_y;
	float temp3_x;
	float temp3_y;

	QPainter painter_bones(this->m_bones);

	painter_bones.setCompositionMode (QPainter::CompositionMode_Clear);
	painter_bones.fillRect(QRectF(0,0,this->m_bones->width(), this->m_bones->height()), Qt::transparent);
	painter_bones.setCompositionMode (QPainter::CompositionMode_SourceOver);

	painter_bones.setBrush(QBrush(Qt::yellow));
	painter_bones.setPen(QColor(255,255,0));

	for(int i = 0; i < (*users).getSize(); i++)
	{
		if((*users)[i].getSkeleton().getState() == nite::SKELETON_NONE)
			this->m_usertracker.startSkeletonTracking((*users)[i].getId());

		if((*users)[i].getSkeleton().getState() == nite::SKELETON_TRACKED)
		{
			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_HEAD).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_NECK).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp2_x = joint_pos_depth_x;
			temp2_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_TORSO).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp2_x, temp2_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp3_x = joint_pos_depth_x;
			temp3_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_SHOULDER).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			painter_bones.drawLine(QPointF(temp2_x, temp2_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_ELBOW).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_HAND).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_SHOULDER).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp2_x, temp2_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_ELBOW).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_HAND).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_HIP).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp3_x, temp3_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_KNEE).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_LEFT_FOOT).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_HIP).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp3_x, temp3_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_KNEE).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));

			temp1_x = joint_pos_depth_x;
			temp1_y = joint_pos_depth_y;

			joint_pos = (*users)[i].getSkeleton().getJoint(nite::JOINT_RIGHT_FOOT).getPosition();
			this->m_usertracker.convertJointCoordinatesToDepth(joint_pos.x, joint_pos.y, joint_pos.z, &joint_pos_depth_x, &joint_pos_depth_y);

			painter_bones.drawEllipse(QPoint(joint_pos_depth_x, joint_pos_depth_y),5,5);

			painter_bones.drawLine(QPointF(temp1_x, temp1_y), QPointF(joint_pos_depth_x, joint_pos_depth_y));
		}
	}

	//this->m_bones = this->m_bones_clone;
}

bool KinectBodyFrameListener::init(openni::Device *device)
{
	if(this->m_usertracker.create(device) == nite::STATUS_OK)
	{
		this->m_usertracker.addNewFrameListener(this);
		this->m_usertracker.setSkeletonSmoothingFactor(0.0f);

		return true;
	}
	else
		return false;
}

void KinectBodyFrameListener::setVideoMode(openni::VideoMode mode)
{
	this->m_usermap = new QImage(mode.getResolutionX(), mode.getResolutionY(), QImage::Format_ARGB32);
	this->m_bones = new QImage(mode.getResolutionX(), mode.getResolutionY(), QImage::Format_ARGB32);
	this->m_bones_clone = new QImage(mode.getResolutionX(), mode.getResolutionY(), QImage::Format_ARGB32);
}

nite::UserTracker *KinectBodyFrameListener::getUserTracker()
{
	return &this->m_usertracker;
}

void KinectBodyFrameListener::destroy()
{
	this->m_usertracker.removeNewFrameListener(this);
	this->m_usertracker.destroy();
	this->m_bones = 0;
	this->m_bones_clone = 0;
	this->m_usermap = 0;
}

QImage *KinectBodyFrameListener::getUserMapImage()
{
	return this->m_usermap;
}

QImage *KinectBodyFrameListener::getBonesImage()
{
	return this->m_bones;
}

