#ifndef KINECTFRAMELISTENER_H
#define KINECTFRAMELISTENER_H

#include <QObject>
#include <QImage>
#include <OpenNI.h>
#include <QDebug>

class KinectFrameListener : public QObject, public openni::VideoStream::NewFrameListener
{
	Q_OBJECT
public:
	explicit KinectFrameListener(QObject *parent = 0);
	void onNewFrame(openni::VideoStream& stream);

private:
	QImage *m_image;
	openni::VideoFrameRef m_frame;
signals:
	void onNewFrame();

public slots:
	QImage *getImage();
	openni::VideoFrameRef *getVideoFrame();
	void init(openni::VideoMode mode);
	void destroy();
};

#endif // KINECTFRAMELISTENER_H
