#ifndef KINECTVIEWER_H
#define KINECTVIEWER_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QBrush>
#include <QImage>
#include <QDebug>
#include <QList>
#include <QString>
#include <QTimer>

#include "kinectframelistener.h"
#include "kinectbodyframelistener.h"

#include <OpenNI.h>
#include <NiTE.h>
#include <NiteEnums.h>

enum KinectSensorType
{
	KINECT_SENSOR_IR = 0,
	KINECT_SENSOR_COLOR = 1,
	KINECT_SENSOR_DEPTH = 2
};

class KinectViewer : public QWidget
{
	Q_OBJECT
public:
	explicit KinectViewer(QWidget *parent = 0);
	~KinectViewer();

protected:
	void paintEvent(QPaintEvent *event) override;

private:
	// OpenNI related
	openni::Device m_device;
	openni::VideoStream m_ir_stream;
	openni::VideoStream m_color_stream;
	openni::VideoStream m_depth_stream;

	// Images related
	QImage m_logo;
	QImage *m_skeletonmap;
	QImage *m_skeletonbones;

	// State related
	bool m_painting;
	bool m_started;
	bool m_opened;

	// Listeners related
	KinectFrameListener m_ir_listener;
	KinectFrameListener m_color_listener;
	KinectFrameListener m_depth_listener;
	KinectBodyFrameListener m_body_listener;

	// Show related
	bool m_showskeletonbones;
	bool m_showskeletonmap;
	KinectSensorType m_current_sensor;

	// Update related
	QTimer m_timer;

signals:

public slots:
	void open(QString device);
	void close();
	void start();
	void stop();
	bool isStarted();
	bool isOpened();
	void setSkeletonSmoothFactor(int value);
	QList<QString> getAvailableDevices();
	QList<QString> getAvailableIRModes();
	QList<QString> getAvailableColorModes();
	QList<QString> getAvailableDepthModes();
	void setIRMode(qint32 index);
	void setColorMode(qint32 index);
	void setDepthMode(qint32 index);
	void setCurrentSensor(KinectSensorType current);
	KinectSensorType getCurrentSensor();
	void setShowSkeletonBones(bool value);
	bool isShowingSkeletonBones();
	void setShowSkeletonMap(bool value);
	bool isShowingSkeletonMap();
	QString pixelFormatToString(openni::PixelFormat format);
	void refresh();

};

#endif // KINECTVIEWER_H
