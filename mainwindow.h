#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QComboBox>
#include <QList>
#include <QString>
#include <QPushButton>
#include "kinectviewer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private:
	Ui::MainWindow *ui;

public slots:
	void quit();
	void startAndStop();
	void connectAndDisconnect();
	void showIRToggled(bool value);
	void showColorToggled(bool value);
	void showDepthToggled(bool value);
	void showSkeletonBonesChanged(bool value);
	void showSkeletonMapChanged(bool value);
};

#endif // MAINWINDOW_H
