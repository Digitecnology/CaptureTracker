#ifndef KINECTBODYFRAMELISTENER_H
#define KINECTBODYFRAMELISTENER_H

#include <QObject>
#include <QImage>
#include <QDebug>
#include <QPainter>
#include <QRgb>

#include <OpenNI.h>
#include <NiTE.h>
#include <NiteEnums.h>

class KinectBodyFrameListener : public QObject, public nite::UserTracker::NewFrameListener
{
	Q_OBJECT
public:
	explicit KinectBodyFrameListener(QObject *parent = 0);
	void onNewFrame (nite::UserTracker &tracker);

private:
	//NiTE related
	nite::UserTracker m_usertracker;

	//Tracking related
	nite::UserTrackerFrameRef m_trackframe;
	QImage *m_usermap;
	QImage *m_bones;
	QImage *m_bones_clone;

signals:

public slots:
	bool init(openni::Device *device);
	void setVideoMode(openni::VideoMode mode);
	nite::UserTracker *getUserTracker();
	void destroy();
	QImage *getUserMapImage();
	QImage *getBonesImage();
};

#endif // KINECTBODYFRAMELISTENER_H
