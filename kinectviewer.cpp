#include "kinectviewer.h"

KinectViewer::KinectViewer(QWidget *parent) : QWidget(parent), m_logo(":/resources/Kinect.png"), m_painting(false), m_started(false), m_opened(false), m_current_sensor(KinectSensorType::KINECT_SENSOR_IR), m_showskeletonbones(false), m_showskeletonmap(false)
{
	openni::OpenNI::initialize();
	nite::NiTE::initialize();
	this->m_timer.setInterval(30);
	this->connect(&this->m_timer, &QTimer::timeout, this, &KinectViewer::refresh);
	this->m_timer.start();
}

KinectViewer::~KinectViewer()
{
	this->m_timer.stop();
	nite::NiTE::shutdown();
	openni::OpenNI::shutdown();
}

void KinectViewer::paintEvent(QPaintEvent *event)
{
	// TODO: Add a image resizing algorithm

	this->m_painting = true;

	if(this->isHidden())
		return;

	QPainter painter(this);

	if(!this->m_started)
	{
		painter.fillRect(0,0,this->width(),this->height(), QColor::fromRgb(0,0,0));
		painter.drawImage(QPointF((this->width() - this->m_logo.width()) / 2.0,(this->height() - this->m_logo.height()) / 2.0),this->m_logo);
	}
	else
	{
		painter.fillRect(0,0,this->width(),this->height(), QColor::fromRgb(0,0,0));

		QImage image;

		switch(this->m_current_sensor)
		{
			case KinectSensorType::KINECT_SENSOR_IR:
				image = this->m_ir_listener.getImage()->scaled(this->width(), this->height(), Qt::KeepAspectRatio);
				painter.drawImage(QPointF((this->width() - image.width()) / 2.0,(this->height() - image.height()) / 2.0),image);
				break;
			case KinectSensorType::KINECT_SENSOR_COLOR:
				image = this->m_color_listener.getImage()->scaled(this->width(), this->height(), Qt::KeepAspectRatio);
				painter.drawImage(QPointF((this->width() - image.width()) / 2.0,(this->height() - image.height()) / 2.0),image);
				break;
			case KinectSensorType::KINECT_SENSOR_DEPTH:
				image = this->m_depth_listener.getImage()->scaled(this->width(), this->height(), Qt::KeepAspectRatio);
				painter.drawImage(QPointF((this->width() - image.width()) / 2.0,(this->height() - image.height()) / 2.0),image);

				if(this->m_showskeletonmap)
				{
					image = this->m_body_listener.getUserMapImage()->scaled(this->width(), this->height(), Qt::KeepAspectRatio);
					painter.drawImage(QPointF((this->width() - image.width()) / 2.0,(this->height() - image.height()) / 2.0),image);
				}

				if(this->m_showskeletonbones)
				{
					image = this->m_body_listener.getBonesImage()->scaled(this->width(), this->height(), Qt::KeepAspectRatio);
					painter.drawImage(QPointF((this->width() - image.width()) / 2.0,(this->height() - image.height()) / 2.0),image);
				}
				break;
		}
	}

	this->m_painting = false;
}

void KinectViewer::open(QString device)
{
	// Open device
	if(this->m_device.open(device.toStdString().c_str()) != openni::DEVICE_STATE_OK)
	{
		this->m_opened = false;
		return;
	}

	// Open IR and Color streams
	if(this->m_ir_stream.create(this->m_device, openni::SENSOR_IR) != openni::STATUS_OK)
	{
		this->m_device.close();
		this->m_opened = false;
		return;
	}

	this->m_ir_stream.addNewFrameListener(&this->m_ir_listener);

	if(this->m_color_stream.create(this->m_device, openni::SENSOR_COLOR) != openni::STATUS_OK)
	{
		this->m_device.close();
		this->m_ir_stream.removeNewFrameListener(&this->m_ir_listener);
		this->m_ir_stream.destroy();
		this->m_opened = false;
		qDebug("ERR");
		return;
	}

	this->m_color_stream.addNewFrameListener(&this->m_color_listener);

	if(this->m_depth_stream.create(this->m_device, openni::SENSOR_DEPTH) != openni::STATUS_OK)
	{
		this->m_device.close();
		this->m_ir_stream.removeNewFrameListener(&this->m_ir_listener);
		this->m_ir_stream.destroy();
		this->m_color_stream.removeNewFrameListener(&this->m_color_listener);
		this->m_color_stream.destroy();
		this->m_opened = false;
		qDebug("ERR2");
		return;
	}

	this->m_depth_stream.addNewFrameListener(&this->m_depth_listener);

	// Create NiTE tracker
	if(!this->m_body_listener.init(&this->m_device))
	{
		this->m_device.close();
		this->m_ir_stream.removeNewFrameListener(&this->m_ir_listener);
		this->m_ir_stream.destroy();
		this->m_color_stream.removeNewFrameListener(&this->m_color_listener);
		this->m_color_stream.destroy();
		this->m_depth_stream.removeNewFrameListener(&this->m_color_listener);
		this->m_depth_stream.destroy();
		this->m_opened = false;
		qDebug("ERR3");
		return;
	}

	this->m_opened = true;
}

void KinectViewer::close()
{
	if(this->m_started)
		this->stop();

	if(!this->m_opened)
		return;

	this->m_ir_stream.removeNewFrameListener(&this->m_ir_listener);
	this->m_color_stream.removeNewFrameListener(&this->m_color_listener);
	this->m_depth_stream.removeNewFrameListener(&this->m_depth_listener);
	this->m_ir_listener.destroy();
	this->m_color_listener.destroy();
	this->m_depth_listener.destroy();
	this->m_body_listener.destroy();
	this->m_ir_stream.destroy();
	this->m_color_stream.destroy();
	this->m_depth_stream.destroy();

	this->m_device.close();
	this->m_opened = false;
}

void KinectViewer::start()
{
	if(!this->m_opened)
		return;

	// Start IR and Color streams
	if(this->m_ir_stream.start() != openni::STATUS_OK)
	{
		this->m_started = false;
		return;
	}

	if(this->m_color_stream.start() != openni::STATUS_OK)
	{
		this->m_ir_stream.stop();
		this->m_started = false;
		return;
	}

	if(this->m_depth_stream.start() != openni::STATUS_OK)
	{
		this->m_ir_stream.stop();
		this->m_color_stream.stop();
		this->m_started = false;
		return;
	}

	this->m_started = true;
}

void KinectViewer::stop()
{
	this->m_ir_stream.stop();
	this->m_color_stream.stop();
	this->m_depth_stream.stop();
	this->m_started = false;

	// Call paint event
	if(!this->isHidden() && !this->m_painting)
		this->update();
}

bool KinectViewer::isStarted()
{
	return this->m_started;
}

bool KinectViewer::isOpened()
{
	return this->m_opened;
}

void KinectViewer::setSkeletonSmoothFactor(int value)
{
	this->m_body_listener.getUserTracker()->setSkeletonSmoothingFactor(value / 100.0);
}

QList<QString> KinectViewer::getAvailableDevices()
{
	QList<QString> devices;

	openni::Array<openni::DeviceInfo> device_infos;

	openni::OpenNI::enumerateDevices(&device_infos);

	for(int i = 0; i < device_infos.getSize(); i++)
		devices.append(QString::fromLocal8Bit(device_infos[i].getUri()));

	return devices;
}

QList<QString> KinectViewer::getAvailableIRModes()
{
	QList<QString> modes;

	if(!this->m_opened)
		return modes;

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_IR);

	const openni::Array<openni::VideoMode> *vmodes = &info->getSupportedVideoModes();

	for(int i = 0; i < vmodes->getSize(); i++)
	{
		modes.append(QString("%1x%2@%3 <%4>").arg(QString::number((*vmodes)[i].getResolutionX()), QString::number((*vmodes)[i].getResolutionY()), QString::number((*vmodes)[i].getFps()), this->pixelFormatToString((*vmodes)[i].getPixelFormat())));
	}


	return modes;
}

QList<QString> KinectViewer::getAvailableColorModes()
{
	QList<QString> modes;

	if(!this->m_opened)
		return modes;

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_COLOR);

	const openni::Array<openni::VideoMode> *vmodes = &info->getSupportedVideoModes();

	for(int i = 0; i < vmodes->getSize(); i++)
	{
		modes.append(QString("%1x%2@%3 <%4>").arg(QString::number((*vmodes)[i].getResolutionX()), QString::number((*vmodes)[i].getResolutionY()), QString::number((*vmodes)[i].getFps()), this->pixelFormatToString((*vmodes)[i].getPixelFormat())));
	}


	return modes;
}

QList<QString> KinectViewer::getAvailableDepthModes()
{
	QList<QString> modes;

	if(!this->m_opened)
		return modes;

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_DEPTH);

	const openni::Array<openni::VideoMode> *vmodes = &info->getSupportedVideoModes();

	for(int i = 0; i < vmodes->getSize(); i++)
	{
		modes.append(QString("%1x%2@%3 <%4>").arg(QString::number((*vmodes)[i].getResolutionX()), QString::number((*vmodes)[i].getResolutionY()), QString::number((*vmodes)[i].getFps()), this->pixelFormatToString((*vmodes)[i].getPixelFormat())));
	}


	return modes;
}

void KinectViewer::setIRMode(qint32 index)
{
	if(!this->m_opened)
		return;

	qDebug() << QString("IR: %1").arg(QString::number(index));

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_IR);

	if (this->m_ir_stream.setVideoMode(info->getSupportedVideoModes()[index]) != openni::STATUS_OK)
	{
		qDebug() << QString("Cannot set IR mode");
		return;
	}

	this->m_ir_listener.init(this->m_ir_stream.getVideoMode());
}

void KinectViewer::setColorMode(qint32 index)
{
	if(!this->m_opened)
		return;

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_COLOR);

	if (this->m_color_stream.setVideoMode(info->getSupportedVideoModes()[index]) != openni::STATUS_OK)
	{
		qDebug() << QString("Cannot set Color mode");
		return;
	}

	this->m_color_listener.init(this->m_color_stream.getVideoMode());
}

void KinectViewer::setDepthMode(qint32 index)
{
	if(!this->m_opened)
		return;

	qDebug() << QString::number(index);

	const openni::SensorInfo *info = this->m_device.getSensorInfo(openni::SENSOR_DEPTH);

	/*if (this->m_depth_stream.setVideoMode(info->getSupportedVideoModes()[index]) != openni::STATUS_OK)
	{
		qDebug() << QString("Cannot set Depth mode");
		return;
	}*/

	this->m_depth_listener.init(this->m_depth_stream.getVideoMode());
	this->m_body_listener.setVideoMode(this->m_depth_stream.getVideoMode());
}

void KinectViewer::setCurrentSensor(KinectSensorType current)
{
	this->m_current_sensor = current;
}

KinectSensorType KinectViewer::getCurrentSensor()
{
	return this->m_current_sensor;
}

void KinectViewer::setShowSkeletonBones(bool value)
{
	this->m_showskeletonbones = value;
}

bool KinectViewer::isShowingSkeletonBones()
{
	return this->m_showskeletonbones;
}

void KinectViewer::setShowSkeletonMap(bool value)
{
	this->m_showskeletonmap = value;
}

bool KinectViewer::isShowingSkeletonMap()
{
	return this->m_showskeletonmap;
}

QString KinectViewer::pixelFormatToString(openni::PixelFormat format)
{
	switch(format)
	{
		case openni::PIXEL_FORMAT_DEPTH_100_UM:
			return QString("Depth 100 UM");
		case openni::PIXEL_FORMAT_DEPTH_1_MM:
			return QString("Depth 1 MM");
		case openni::PIXEL_FORMAT_GRAY8:
			return QString("Gray 8");
		case openni::PIXEL_FORMAT_GRAY16:
			return QString("Gray 16");
		case openni::PIXEL_FORMAT_JPEG:
			return QString("JPEG");
		case openni::PIXEL_FORMAT_RGB888:
			return QString("RGB 888");
		case openni::PIXEL_FORMAT_SHIFT_9_2:
			return QString("Shift 92");
		case openni::PIXEL_FORMAT_SHIFT_9_3:
			return QString("Shift 93");
		case openni::PIXEL_FORMAT_YUV422:
			return QString("YUV 422");
		case openni::PIXEL_FORMAT_YUYV:
			return QString("YUYV");
	}
}

void KinectViewer::refresh()
{
	this->update();
}
